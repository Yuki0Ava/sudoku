import curses

BLUE = 1
CYAN = 2
GREEN = 3
MAGENTA = 4
RED = 5
WHITE = 6
YELLOW = 7

colors = {
    BLUE:       curses.COLOR_BLUE,
    CYAN:       curses.COLOR_CYAN,
    GREEN:      curses.COLOR_GREEN,
    MAGENTA:    curses.COLOR_MAGENTA,
    RED:        curses.COLOR_RED,
    WHITE:      curses.COLOR_WHITE,
    YELLOW:     curses.COLOR_YELLOW}

class SudokuUI:
    
    def __init__(self, stdscr, draw_possible = False, display_messages = False):
        stdscr.clear()
        
        curses.curs_set(0)
        
        self.problem_window = curses.newwin(13, 19, 0, 0)
        self.problem_window.border()
        self.draw_problem_grid()
        self.problem_window.refresh()
        
        if draw_possible:
            self.candidates_window = curses.newwin(37, 57, 0, 20)
            self.candidates_window.border()
            self.draw_candidates_grid()
            self.candidates_window.refresh()
            
        if display_messages:
            self.message_window = curses.newwin(20, 19, 13, 0)
            self.message_window.border()
            self.message_window.refresh()
            self.message_line = 0
            
        for number, val in colors.items():
            curses.init_pair(number, colors[number], curses.COLOR_BLACK)
        
    def draw_problem_grid(self):
        for row in range(1, 12):
            if row % 4 != 0:
                self.problem_window.addstr(row, 6, '|')
                self.problem_window.addstr(row, 12, '|')
            else:
                self.problem_window.addstr(row, 1, '-----+'*2 + '-----')
        self.problem_window.refresh()
        
    def draw_candidates_grid(self):
        for row in range(1,36):
            if row not in [4, 8, 12, 16, 20, 24, 28, 32]:
                for col in [6, 12, 18, 19, 25, 31, 37, 38, 44, 50]:
                    self.candidates_window.addstr(row, col, '|')
            elif row == 12 or row == 24:
                self.candidates_window.addstr(row, 1, ('=====+'*3 + '+')*2 + '=====+'*2 + '=====')
            else:
                self.candidates_window.addstr(row, 1, ('-----+'*3 + '+')*2 + '-----+'*2 + '-----')
        self.candidates_window.refresh()
        
    def update_problem(self, row, col, value, color = None):
        if value == 0:
            return
        if color == None:
            color = 0
        self.problem_window.addstr(row + 1 + row//3, 2*col + 1, str(value), curses.color_pair(color))
        self.problem_window.refresh()
        
    def update_candidates(self, row, col, values, color = None):
        if color == None:
            color = 0
            
        block_row = 4*row + 1
        block_col = 6*col + 1 + col//3
        
        for subrow in range(3):
            for subcol in range(3):
                value = subrow*3 + subcol + 1
                if value in values:
                    self.candidates_window.addstr(block_row + subrow, block_col + 2*subcol, str(value), curses.color_pair(color))
                else:
                    self.candidates_window.addstr(block_row + subrow, block_col + 2*subcol, ' ')
        self.candidates_window.refresh()
        
    def print_problem(self, grid, color = None):
        for row in range(9):
            for col in range(9):
                self.update_problem(row, col, grid[row][col], color)
                
    def print_candidates(self, grid, candidates, color = None):
        for row in range(9):
            for col in range(9):
                if grid[row][col] != 0:
                    values = [grid[row][col]]
                    self.update_candidates(row, col, values, GREEN)
                else:
                    values = candidates[row][col]
                    self.update_candidates(row, col, values, color)
        
    def wait_for_input(self):
        self.problem_window.getch()
        
        
    def output(self, msg):
        
        cols = 19
        lines = 20
            
        while msg:           
            
            if self.message_line == (lines - 2):
                self.message_window.move(1,1)
                self.message_window.deleteln()
                self.message_window.move(self.message_line, 1)
                self.message_window.addstr(" " * (cols - 2))
                self.message_window.border()
            else:
                self.message_line += 1

            if len(msg) > cols - 2:
                part = msg[:cols - 1]
                split = part.rfind(' ')
                #if split == -1:
                #    split = cols - 2
                line = part[:split]
                msg = msg[len(line):]
                msg = msg.lstrip()
            else:
                line = msg
                msg = None

            self.message_window.move(self.message_line, 1)
            self.message_window.addstr(line, WHITE)
            self.message_window.refresh()
        
        
        
        
